// Enable/Disable DesignPreview
const enableDesignPreview = true && Boolean(location.protocol === "http:");
console.log(`= Design preview (enable: ${enableDesignPreview}) =`);

// Create elements
const dpDesktopIcon = document.createElement("img");
dpDesktopIcon.setAttribute("id", "design-preview-desktop-icon");
dpDesktopIcon.setAttribute("src", "./design/desktop-icon.svg");
dpDesktopIcon.setAttribute("alt", "desktop-icon");
dpDesktopIcon.setAttribute("title", "desktop-icon");
dpDesktopIcon.classList.add("design-preview-icon");
dpDesktopIcon.setAttribute("data-preview-icon-enable", "false");

const dpMobileIcon = document.createElement("img");
dpMobileIcon.setAttribute("id", "design-preview-mobile-icon");
dpMobileIcon.setAttribute("src", "./design/mobile-icon.svg");
dpMobileIcon.setAttribute("alt", "mobile-icon");
dpMobileIcon.setAttribute("title", "mobile-icon");
dpMobileIcon.classList.add("design-preview-icon");
dpMobileIcon.setAttribute("data-preview-icon-enable", "false");

const dpCancelIcon = document.createElement("img");
dpCancelIcon.setAttribute("id", "design-preview-cancel-icon");
dpCancelIcon.setAttribute("src", "./design/cancel-icon.svg");
dpCancelIcon.setAttribute("alt", "cancel-icon");
dpCancelIcon.setAttribute("title", "cancel-icon");
dpCancelIcon.classList.add("design-preview-icon");
dpCancelIcon.setAttribute("data-preview-icon-enable", "true");

const dpIconsContainer = document.createElement("div");
dpIconsContainer.id = "design-preview-icons-container";
dpIconsContainer.appendChild(dpDesktopIcon);
dpIconsContainer.appendChild(dpMobileIcon);
dpIconsContainer.appendChild(dpCancelIcon);

const dpDesktopDesign = document.createElement("img");
dpDesktopDesign.setAttribute("id", "design-preview-desktop-design");
dpDesktopDesign.setAttribute("src", "./design/desktop-design.jpg");
dpDesktopDesign.setAttribute("alt", "desktop-design");
dpDesktopDesign.setAttribute("title", "desktop-design");
dpDesktopDesign.classList.add("design-preview-design");
dpDesktopDesign.setAttribute("data-preview-design-enable", "false");

const dpMobileDesign = document.createElement("img");
dpMobileDesign.setAttribute("id", "design-preview-mobile-design");
dpMobileDesign.setAttribute("src", "./design/mobile-design.jpg");
dpMobileDesign.setAttribute("alt", "mobile-design");
dpMobileDesign.setAttribute("title", "mobile-design");
dpMobileDesign.classList.add("design-preview-design");
dpMobileDesign.setAttribute("data-preview-design-enable", "false");

const dpDesignsContainer = document.createElement("div");
dpDesignsContainer.id = "design-preview-designs-container";
dpDesignsContainer.appendChild(dpDesktopDesign);
dpDesignsContainer.appendChild(dpMobileDesign);

// Add to DOM
if (enableDesignPreview) {
  document.body.appendChild(dpIconsContainer);
  document.body.appendChild(dpDesignsContainer);
}

// Add event listeners

// Click on desktop design
dpDesktopIcon.addEventListener("click", () => {
  dpDesktopIcon.setAttribute("data-preview-icon-enable", "true");
  dpMobileIcon.setAttribute("data-preview-icon-enable", "false");
  dpCancelIcon.setAttribute("data-preview-icon-enable", "false");

  dpDesktopDesign.setAttribute("data-preview-design-enable", "true");
  dpMobileDesign.setAttribute("data-preview-design-enable", "false");
});

// Click on mobile design
dpMobileIcon.addEventListener("click", () => {
  dpDesktopIcon.setAttribute("data-preview-icon-enable", "false");
  dpMobileIcon.setAttribute("data-preview-icon-enable", "true");
  dpCancelIcon.setAttribute("data-preview-icon-enable", "false");

  dpDesktopDesign.setAttribute("data-preview-design-enable", "false");
  dpMobileDesign.setAttribute("data-preview-design-enable", "true");
});

// Click on cancel design
dpCancelIcon.addEventListener("click", () => {
  dpDesktopIcon.setAttribute("data-preview-icon-enable", "false");
  dpMobileIcon.setAttribute("data-preview-icon-enable", "false");
  dpCancelIcon.setAttribute("data-preview-icon-enable", "true");

  dpDesktopDesign.setAttribute("data-preview-design-enable", "false");
  dpMobileDesign.setAttribute("data-preview-design-enable", "false");
});

// Disable scroll over design menu
dpIconsContainer.addEventListener("wheel", (event) => {
  event.preventDefault();
  event.stopPropagation();
});

// Scroll over Desktop design icon
dpDesktopIcon.addEventListener("wheel", (event) => {
  let currentOpacity = window.getComputedStyle(dpDesktopDesign).opacity;
  // console.log(currentOpacity);
  let direction = Number(event.deltaY) < 0 ? "up" : "down";

  if (direction === "up") {
    // Increase opacity
    dpDesktopDesign.style.opacity = Number(currentOpacity) + 0.1;
  }
  if (direction === "down") {
    // Reduce opacity
    dpDesktopDesign.style.opacity = Number(currentOpacity) - 0.1;
  }
});

// Scroll over Mobile design icon
dpMobileIcon.addEventListener("wheel", (event) => {
  let currentOpacity = window.getComputedStyle(dpMobileDesign).opacity;
  // console.log(currentOpacity);
  let direction = Number(event.deltaY) < 0 ? "up" : "down";

  if (direction === "up") {
    // Increase opacity
    dpMobileDesign.style.opacity = Number(currentOpacity) + 0.1;
  }
  if (direction === "down") {
    // Reduce opacity
    dpMobileDesign.style.opacity = Number(currentOpacity) - 0.1;
  }
});
